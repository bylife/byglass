# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150827122627) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string  "name"
    t.boolean "capital"
    t.integer "state_id"
  end

  create_table "customers", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "rg_ie"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "entity"
    t.string   "cpf_cnpj"
    t.string   "neighborhood"
    t.string   "cep"
    t.string   "contact"
  end

  create_table "glasses", force: true do |t|
    t.string   "title"
    t.decimal  "cost"
    t.float    "markup"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "thickness"
  end

  create_table "glasses_projects", force: true do |t|
    t.integer  "glasses_id"
    t.integer  "projects_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "graphics", force: true do |t|
    t.string   "title"
    t.string   "path"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hardwares", force: true do |t|
    t.string   "title"
    t.decimal  "cost"
    t.float    "markup"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hardwares_projects", force: true do |t|
    t.integer  "hardwares_id"
    t.integer  "projects_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "measures", force: true do |t|
    t.string   "title"
    t.float    "value"
    t.string   "unity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "title"
    t.integer  "type_id"
    t.text     "description"
    t.float    "width"
    t.float    "height"
    t.float    "weight"
    t.decimal  "cost"
    t.float    "markup"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.integer  "stock"
  end

  create_table "products_sales", force: true do |t|
    t.integer  "porduct_id"
    t.integer  "sale_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", force: true do |t|
    t.string   "title"
    t.string   "code"
    t.integer  "category_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects_measures", force: true do |t|
    t.integer  "projects_id"
    t.integer  "measures_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects_shapes", force: true do |t|
    t.integer  "projects_id"
    t.integer  "shapes_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales", force: true do |t|
    t.integer  "customer_id"
    t.integer  "user_id"
    t.float    "value"
    t.boolean  "delivered"
    t.string   "observations"
    t.integer  "payment_method"
    t.boolean  "payed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shapes", force: true do |t|
    t.string   "title"
    t.decimal  "cost"
    t.float    "markup"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "acronym"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "types", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "password"
    t.string   "confirmation"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
