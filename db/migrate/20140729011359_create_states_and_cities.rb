class CreateStatesAndCities < ActiveRecord::Migration
  def change
  	create_table :states do |t|
  		t.string :name
  		t.string :acronym

  		t.timestamps
  	end
  	create_table :cities do |t|
  		t.string :name
  		t.boolean :capital
  		t.integer :state_id
  	end
  end
end
