class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :code
      t.integer :category_id
      t.string :description

      t.timestamps
    end
  end
end
