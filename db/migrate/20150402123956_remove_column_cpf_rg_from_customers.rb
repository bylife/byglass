class RemoveColumnCpfRgFromCustomers < ActiveRecord::Migration
  def change
  	rename_column :customers, :cpf_cnpf, :cpf_cnpj
  	remove_column :customers, :cpf
  	rename_column :customers, :rg, :rg_ie
  end
end
