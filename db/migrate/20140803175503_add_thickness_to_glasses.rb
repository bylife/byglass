class AddThicknessToGlasses < ActiveRecord::Migration
  def change
  	add_column :glasses, :thickness, :decimal
  end
end
