class CreateTableGlassesProjects < ActiveRecord::Migration
  def change
    create_table :glasses_projects do |t|
    	t.belongs_to :glasses
    	t.belongs_to :projects

    	t.timestamps
    end

    create_table :hardwares_projects do |t|
    	t.belongs_to :hardwares
    	t.belongs_to :projects

    	t.timestamps
    end

    create_table :projects_shapes do |t|
    	t.belongs_to :projects
    	t.belongs_to :shapes

    	t.timestamps
    end

    create_table :projects_measures do |t|
    	t.belongs_to :projects
    	t.belongs_to :measures

    	t.timestamps
    end
  end
end
