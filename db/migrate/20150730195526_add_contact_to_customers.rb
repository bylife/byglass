class AddContactToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :contact, :string
  end
end
