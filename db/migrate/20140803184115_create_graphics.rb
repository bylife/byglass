class CreateGraphics < ActiveRecord::Migration
  def change
    create_table :graphics do |t|
      t.string :title
      t.string :path

      t.timestamps
    end
  end
end
