class AddAddressFieldsToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :entity, :integer
    add_column :customers, :cpf_cnpf, :string
    add_column :customers, :neighborhood, :string
    add_column :customers, :cep, :string
  end
end
