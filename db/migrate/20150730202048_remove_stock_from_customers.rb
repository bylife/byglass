class RemoveStockFromCustomers < ActiveRecord::Migration
  def change
  	remove_column :customers, :stock
  end
end
