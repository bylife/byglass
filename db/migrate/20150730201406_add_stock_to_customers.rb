class AddStockToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :stock, :integer
  end
end
