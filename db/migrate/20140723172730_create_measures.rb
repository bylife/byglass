class CreateMeasures < ActiveRecord::Migration
  def change
    create_table :measures do |t|
      t.string :title
      t.float :value
      t.string :unity

      t.timestamps
    end
  end
end
