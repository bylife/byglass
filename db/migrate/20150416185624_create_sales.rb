class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :customer_id
      t.integer :user_id
      t.float :value
      t.boolean :delivered
      t.string :observations
      t.integer :payment_method
      t.boolean :payed

      t.timestamps
    end
  end
end
