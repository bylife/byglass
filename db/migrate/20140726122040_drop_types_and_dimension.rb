class DropTypesAndDimension < ActiveRecord::Migration
  def drop
  	drop_table :types
  	drop_table :dimensions
  end
end
