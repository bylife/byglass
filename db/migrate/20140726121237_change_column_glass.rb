class ChangeColumnGlass < ActiveRecord::Migration
  def change
  	rename_column :glasses, :type, :title	
  	rename_column :hardwares, :type, :title	
  	rename_column :shapes, :type, :title	
  end
end
