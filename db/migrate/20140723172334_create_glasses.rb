class CreateGlasses < ActiveRecord::Migration
  def change
    create_table :glasses do |t|
      t.string :type
      t.decimal :cost
      t.float :markup
      t.decimal :value

      t.timestamps
    end
  end
end
