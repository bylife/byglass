class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.integer :type_id
      t.text :description
      t.float :width
      t.float :height
      t.float :weight
      t.decimal :cost
      t.float :markup
      t.decimal :value

      t.timestamps
    end
  end
end
