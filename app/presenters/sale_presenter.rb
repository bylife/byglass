class SalePresenter < Burgundy::Item

	def user_name
		if item.user.present?
			item.user.name
		end
	end

	def payed?
		if item.payed
			'Pago!'
		else
			'Não Pago'
		end
	end

	def delivered?
		if item.payed
			'Entregue!'
		else
			'Não Entregue'
		end
	end

	def payment_method
		case item.payment_method
		when 1
			"Cartão de crédito"
		when 2
			"Cartão de débito"
		when 3
			"Dinheiro"
		when 4
			"Cheque"			
		end
	end
end
