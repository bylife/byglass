class SessionsController < ApplicationController
  def new	
  end

  def create
  	#render: params.inspect
	super do |resource|
      #render resource.inspect
      sign_in_and_redirect(resource)
      respond_with(resource, serialize_options(resource))
    end
  end

  def destroy
  	session[:user_id] = nil
  	redirect_to login_path
  end
end
