class SalesController < ApplicationController
  before_action :set_sale, only: [:show, :edit, :update, :destroy]
  decorates_assigned :sale
  # GET /sales
  # GET /sales.json
  def index
    sales = Sale.all    
    @sales = sales.map do |sale|
      sale.decorate
    end  
  end

  # GET /sales/1
  # GET /sales/1.json
  def show
  end

  # GET /sales/new
  def new
    @sale = Sale.new
    @payment_methods = [ ["Cartão de crédito", 1], ["Cartão de débito", 2], ["Dinheiro", 3], ["Cheque", 4] ]
    @products = Product.all
  end

  # GET /sales/1/edit
  def edit
    @payment_methods = [ ["Cartão de crédito", 1], ["Cartão de débito", 2], ["Dinheiro", 3], ["Cheque", 4] ]
  end

  # POST /sales
  # POST /sales.json
  def create
    @sale = Sale.new(sale_params)

    respond_to do |format|
      if @sale.save
        format.html { redirect_to @sale, notice: 'Sale was successfully created.' }
        format.json { render action: 'show', status: :created, location: @sale }
      else
        format.html { render action: 'new' }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales/1
  # PATCH/PUT /sales/1.json
  def update
    respond_to do |format|
      if @sale.update(sale_params)
        format.html { redirect_to @sale, notice: 'Sale was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales/1
  # DELETE /sales/1.json
  def destroy
    @sale.destroy
    respond_to do |format|
      format.html { redirect_to sales_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sale
      @sale = Sale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sale_params
      params.require(:sale).permit(:customer_id, :user_id, :value, :delivered, :observations, :payment_method, :payed, :product)
    end
end
