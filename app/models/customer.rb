class Customer < ActiveRecord::Base
	EMAIL_REGEXP = /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/

	has_many :sale

	validates_presence_of :name
	validates_format_of :email, with: EMAIL_REGEXP, uniqueness: true
end
