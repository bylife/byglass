class Project < ActiveRecord::Base
	belongs_to :categories
	has_and_belongs_to_many :hardwares
	has_and_belongs_to_many :measures
	has_and_belongs_to_many :glasses
	has_and_belongs_to_many :shapes	

	validates_presence_of :title, :code, :category_id
	validates_length_of :description, minimum: 10, maximum: 600, allow_blank: false
end
