class Sale < ActiveRecord::Base
	belongs_to :customer
	belongs_to :user
	has_and_belongs_to_many :product
	validates_presence_of :customer_id, :value, :payment_method
end
