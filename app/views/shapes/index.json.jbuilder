json.array!(@shapes) do |shape|
  json.extract! shape, :id, :title, :cost, :markup, :value
  json.url shape_url(shape, format: :json)
end
