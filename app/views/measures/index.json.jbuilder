json.array!(@measures) do |measure|
  json.extract! measure, :id, :title, :value, :unity
  json.url measure_url(measure, format: :json)
end
