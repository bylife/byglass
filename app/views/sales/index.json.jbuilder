json.array!(@sales) do |sale|
  json.extract! sale, :id, :customer_id, :user_id, :value, :delivered, :observations, :payment_method, :payed
  json.url sale_url(sale, format: :json)
end
