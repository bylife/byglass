json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :entity, :email, :address, :city, :state, :neighborhood, :phone, :cellphone, :cpf_cnpj, :rg_ie, :created_at, :updated_at
  json.url customer_url(customer, format: :json)
end
