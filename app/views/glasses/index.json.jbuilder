json.array!(@glasses) do |glass|
  json.extract! glass, :id, :title, :cost, :markup, :value
  json.url glass_url(glass, format: :json)
end
