json.array!(@products) do |product|
  json.extract! product, :id, :code, :title, :type_id, :description, :stock, :width, :height, :weight, :cost, :markup, :value
  json.url product_url(product, format: :json)
end
