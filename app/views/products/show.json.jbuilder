json.extract! @product, :id, :code, :title, :type_id, :description, :width, :height, :weight, :cost, :markup, :value, :created_at, :updated_at
