json.array!(@graphics) do |graphic|
  json.extract! graphic, :id, :title, :path
  json.url graphic_url(graphic, format: :json)
end
