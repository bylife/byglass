json.array!(@hardwares) do |hardware|
  json.extract! hardware, :id, :title, :cost, :markup, :value
  json.url hardware_url(hardware, format: :json)
end
