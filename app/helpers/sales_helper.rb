module SalesHelper
	def which_method(method)
		case method
		when 1
			return "Cartão de crédito"
		when 2
			return "Cartão de débito"
		when 3
			return "Dinheiro"
		when 4
			return "Cheque"			
		end
	end

	def is_payed(value)
		case value
		when true
			return "Pago!"
		when false
			return "Não pago."
		end
	end

	def is_delivered(value)
		case value
		when true
			return "Entregue!"
		when false
			return "Não entregue."
		end
	end

	def user_name(user)
		if user.present?
			user.name
		end
	end
end
