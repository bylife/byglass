class SaleDecorator < Draper::Decorator
  delegate_all

  def published_at
    object.published_at.strftime("%A, %B %e")
  end

  def user_name
		if object.user.present?
			object.user.name
		end
	end

	def payed?
		if object.payed
			'Pago!'
		else
			'Não Pago'
		end
	end

	def delivered?
		if object.payed
			'Entregue!'
		else
			'Não Entregue'
		end
	end

	def payment_method
		case object.payment_method
		when 1
			"Cartão de crédito"
		when 2
			"Cartão de débito"
		when 3
			"Dinheiro"
		when 4
			"Cheque"			
		end
	end

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end
