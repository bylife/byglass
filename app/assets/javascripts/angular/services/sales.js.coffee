Global.byglass.factory 'Sale', ['$rootScope', '$http', '$resource', ($rootScope, $http, $resource) -> 
	$resource(
		$rootScope.apiUrl + 'sales/:id.json', { id: '@id'},
		{
			'index'		: method: 'GET', 	isArray : true
			'show'		: method: 'GET', 	isArray : false
			'create'	: method: 'POST'
			'update'	: method: 'PUT'
			'destroy'	: method: 'DELETE'
		}
	)
]