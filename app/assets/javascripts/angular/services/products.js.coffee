Global.byglass.factory 'Product', ['$rootScope', '$http', '$resource', ($rootScope, $http, $resource) -> 
	$resource(
		$rootScope.apiUrl + 'products/:id.json', { id: '@id'},
		{
			'index'		: method: 'GET', 	isArray : true
			'show'		: method: 'GET', 	isArray : false
			'create'	: method: 'POST'
			'update'	: method: 'PUT'
			'destroy'	: method: 'DELETE'
		}
	)
]