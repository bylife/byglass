@SaleCtrl = ['$scope', '$location', '$http', ($scope, $location, $http) ->
	$scope.sales = []
	$http.get('/sales.json').success((data) ->
		$scope.sales = data
	)
]

@SaleNewCtrl = ['$scope', '$location', 'Sale', '$http', ($scope, $location, Sale, $http) ->
	$scope.product 		= []
	$scope.Sale 		= {}
	$scope.Sale.value 	= 0
	$scope.Sale.Product = []

	$http.get('/products.json').success((data) ->
		$scope.products = data
	)
	$http.get('/customers.json').success((data) ->
		$scope.customers = data
	)
	$scope.payment_methods = [
		id: 1, title: 'Cartão de Crédito'
	,
		id: 2, title: 'Cartão de Débito'
	,
		id: 3, title: 'Dinheiro'
	,
		id: 4, title: 'Cheque'
	]

	$scope.saveSale = () ->
		#provisório
		console.log $scope.Sale
		Sale.create({sale: $scope.Sale}
			, () ->
				console.log 'foi'
			, (error) ->
				console.log error
		)
		###$http.post('/sales.json', {sale:$scope.Sale, product: $scope.Sale.Product} ).then((response) ->
			console.log response
		)###

	$scope.calcTotal = (value) ->
		$scope.Sale.value = $scope.Sale.value + value

	product_add = (item) ->
		$scope.Sale.Product.push(item)
		$scope.calcTotal(item.value)

	$scope.removeProduct = ($index) ->
		$scope.products.splice($index, 1)

	$scope.onSelectProduct = (item, model, label) ->
		product_add(item)
		$scope.product_add = null
]