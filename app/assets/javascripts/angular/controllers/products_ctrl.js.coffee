@ProductCtrl = ['$scope', '$location', '$http', ($scope, $location, $http) ->
	$scope.products = []
	$http.get('/products.json').success((data) ->
		$scope.products = data
	)
]

@ProductNewCtrl = ['$scope', '$location', '$http', ($scope, $location, $http) -> 
	$scope.Product 			= {}
	$scope.Product.cost 	= 0
	$scope.Product.markup 	= 25.00

	$scope.changeValue = () ->
		$scope.Product.value 	= $scope.Product.cost * ($scope.Product.markup / 100)

	$scope.changeMarkup = () ->
		$scope.Product.markup 	= 100 * ($scope.Product.value - $scope.Product.cost)/$scope.Product.cost


]