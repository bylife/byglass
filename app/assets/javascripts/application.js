// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require angular
//= require angular-resource
//= require angular-ui-utils/modules/utils
//= require angular-ui-bootstrap-bower/ui-bootstrap.min
//= require angular-ui-bootstrap-bower/ui-bootstrap-tpls.min
//= require angular-ui-mask/dist/mask.min
//= require angular-route/angular-route.min
//= require jquery
//= require jquery_ujs
//= require bootstrap/bootstrap
//= require main.js
//= require_tree ./angular
