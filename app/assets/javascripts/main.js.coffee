if !window.Global
    window.Global = {}

if !Global.angular_dependencies
  Global.angular_dependencies = ['ngResource', 'ngRoute', 'ui.mask', 'ui.bootstrap', 'ui.bootstrap.tpls']

Global.byglass = angular.module 'byglass', Global.angular_dependencies

Global.byglass.config(['$routeProvider', '$httpProvider', '$locationProvider', ($routeProvider, $httpProvider, $locationProvider) ->
  authToken = $("meta[name=\"csrf-token\"]").attr("content")
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken  
])

Global.byglass.controller 'AppCtrl', ['$rootScope', '$scope', ($rootScope, $scope) ->
	$rootScope.apiUrl = 'http://localhost:3000/'
]