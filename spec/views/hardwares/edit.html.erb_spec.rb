require 'spec_helper'

describe "hardwares/edit" do
  before(:each) do
    @hardware = assign(:hardware, stub_model(Hardware,
      :type => "",
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ))
  end

  it "renders the edit hardware form" do
    render

    assert_select "form[action=?][method=?]", hardware_path(@hardware), "post" do
      assert_select "input#hardware_type[name=?]", "hardware[type]"
      assert_select "input#hardware_cost[name=?]", "hardware[cost]"
      assert_select "input#hardware_markup[name=?]", "hardware[markup]"
      assert_select "input#hardware_value[name=?]", "hardware[value]"
    end
  end
end
