require 'spec_helper'

describe "products/index" do
  before(:each) do
    assign(:products, [
      stub_model(Product,
        :title => "Title",
        :type_id => 1,
        :description => "MyText",
        :width => 1.5,
        :height => 1.5,
        :weight => 1.5,
        :cost => "9.99",
        :markup => 1.5,
        :value => "9.99"
      ),
      stub_model(Product,
        :title => "Title",
        :type_id => 1,
        :description => "MyText",
        :width => 1.5,
        :height => 1.5,
        :weight => 1.5,
        :cost => "9.99",
        :markup => 1.5,
        :value => "9.99"
      )
    ])
  end

  it "renders a list of products" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
