require 'spec_helper'

describe "products/edit" do
  before(:each) do
    @product = assign(:product, stub_model(Product,
      :title => "MyString",
      :type_id => 1,
      :description => "MyText",
      :width => 1.5,
      :height => 1.5,
      :weight => 1.5,
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ))
  end

  it "renders the edit product form" do
    render

    assert_select "form[action=?][method=?]", product_path(@product), "post" do
      assert_select "input#product_title[name=?]", "product[title]"
      assert_select "input#product_type_id[name=?]", "product[type_id]"
      assert_select "textarea#product_description[name=?]", "product[description]"
      assert_select "input#product_width[name=?]", "product[width]"
      assert_select "input#product_height[name=?]", "product[height]"
      assert_select "input#product_weight[name=?]", "product[weight]"
      assert_select "input#product_cost[name=?]", "product[cost]"
      assert_select "input#product_markup[name=?]", "product[markup]"
      assert_select "input#product_value[name=?]", "product[value]"
    end
  end
end
