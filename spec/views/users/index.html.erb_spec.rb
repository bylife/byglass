require 'spec_helper'

describe "users/index" do
  before(:each) do
    assign(:users, [
      stub_model(User,
        :name => "Name",
        :email => "Email",
        :password => "Password",
        :confirmation => "Confirmation",
        :active => false
      ),
      stub_model(User,
        :name => "Name",
        :email => "Email",
        :password => "Password",
        :confirmation => "Confirmation",
        :active => false
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Password".to_s, :count => 2
    assert_select "tr>td", :text => "Confirmation".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
