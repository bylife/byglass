require 'spec_helper'

describe "types/edit" do
  before(:each) do
    @type = assign(:type, stub_model(Type,
      :title => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit type form" do
    render

    assert_select "form[action=?][method=?]", type_path(@type), "post" do
      assert_select "input#type_title[name=?]", "type[title]"
      assert_select "input#type_description[name=?]", "type[description]"
    end
  end
end
