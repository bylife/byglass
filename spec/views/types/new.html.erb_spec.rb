require 'spec_helper'

describe "types/new" do
  before(:each) do
    assign(:type, stub_model(Type,
      :title => "MyString",
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new type form" do
    render

    assert_select "form[action=?][method=?]", types_path, "post" do
      assert_select "input#type_title[name=?]", "type[title]"
      assert_select "input#type_description[name=?]", "type[description]"
    end
  end
end
