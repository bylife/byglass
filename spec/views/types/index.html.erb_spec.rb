require 'spec_helper'

describe "types/index" do
  before(:each) do
    assign(:types, [
      stub_model(Type,
        :title => "Title",
        :description => "Description"
      ),
      stub_model(Type,
        :title => "Title",
        :description => "Description"
      )
    ])
  end

  it "renders a list of types" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
