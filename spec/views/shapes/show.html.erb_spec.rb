require 'spec_helper'

describe "shapes/show" do
  before(:each) do
    @shape = assign(:shape, stub_model(Shape,
      :type => "Type",
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Type/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/9.99/)
  end
end
