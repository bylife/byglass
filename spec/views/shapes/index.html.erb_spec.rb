require 'spec_helper'

describe "shapes/index" do
  before(:each) do
    assign(:shapes, [
      stub_model(Shape,
        :type => "Type",
        :cost => "9.99",
        :markup => 1.5,
        :value => "9.99"
      ),
      stub_model(Shape,
        :type => "Type",
        :cost => "9.99",
        :markup => 1.5,
        :value => "9.99"
      )
    ])
  end

  it "renders a list of shapes" do
    render
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
