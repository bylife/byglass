require 'spec_helper'

describe "shapes/new" do
  before(:each) do
    assign(:shape, stub_model(Shape,
      :type => "",
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new shape form" do
    render

    assert_select "form[action=?][method=?]", shapes_path, "post" do
      assert_select "input#shape_type[name=?]", "shape[type]"
      assert_select "input#shape_cost[name=?]", "shape[cost]"
      assert_select "input#shape_markup[name=?]", "shape[markup]"
      assert_select "input#shape_value[name=?]", "shape[value]"
    end
  end
end
