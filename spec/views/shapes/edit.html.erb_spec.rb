require 'spec_helper'

describe "shapes/edit" do
  before(:each) do
    @shape = assign(:shape, stub_model(Shape,
      :type => "",
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ))
  end

  it "renders the edit shape form" do
    render

    assert_select "form[action=?][method=?]", shape_path(@shape), "post" do
      assert_select "input#shape_type[name=?]", "shape[type]"
      assert_select "input#shape_cost[name=?]", "shape[cost]"
      assert_select "input#shape_markup[name=?]", "shape[markup]"
      assert_select "input#shape_value[name=?]", "shape[value]"
    end
  end
end
