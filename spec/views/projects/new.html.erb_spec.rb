require 'spec_helper'

describe "projects/new" do
  before(:each) do
    assign(:project, stub_model(Project,
      :title => "MyString",
      :code => "MyString",
      :category_id => 1,
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new project form" do
    render

    assert_select "form[action=?][method=?]", projects_path, "post" do
      assert_select "input#project_title[name=?]", "project[title]"
      assert_select "input#project_code[name=?]", "project[code]"
      assert_select "input#project_category_id[name=?]", "project[category_id]"
      assert_select "input#project_description[name=?]", "project[description]"
    end
  end
end
