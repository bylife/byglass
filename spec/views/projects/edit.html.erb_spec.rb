require 'spec_helper'

describe "projects/edit" do
  before(:each) do
    @project = assign(:project, stub_model(Project,
      :title => "MyString",
      :code => "MyString",
      :category_id => 1,
      :description => "MyString"
    ))
  end

  it "renders the edit project form" do
    render

    assert_select "form[action=?][method=?]", project_path(@project), "post" do
      assert_select "input#project_title[name=?]", "project[title]"
      assert_select "input#project_code[name=?]", "project[code]"
      assert_select "input#project_category_id[name=?]", "project[category_id]"
      assert_select "input#project_description[name=?]", "project[description]"
    end
  end
end
