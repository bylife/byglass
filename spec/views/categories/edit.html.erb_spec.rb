require 'spec_helper'

describe "categories/edit" do
  before(:each) do
    @category = assign(:category, stub_model(Category,
      : => "",
      : => ""
    ))
  end

  it "renders the edit category form" do
    render

    assert_select "form[action=?][method=?]", category_path(@category), "post" do
      assert_select "input#category_[name=?]", "category[]"
      assert_select "input#category_[name=?]", "category[]"
    end
  end
end
