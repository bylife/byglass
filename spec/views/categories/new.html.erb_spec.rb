require 'spec_helper'

describe "categories/new" do
  before(:each) do
    assign(:category, stub_model(Category,
      : => "",
      : => ""
    ).as_new_record)
  end

  it "renders new category form" do
    render

    assert_select "form[action=?][method=?]", categories_path, "post" do
      assert_select "input#category_[name=?]", "category[]"
      assert_select "input#category_[name=?]", "category[]"
    end
  end
end
