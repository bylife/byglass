require 'spec_helper'

describe "categories/index" do
  before(:each) do
    assign(:categories, [
      stub_model(Category,
        : => "",
        : => ""
      ),
      stub_model(Category,
        : => "",
        : => ""
      )
    ])
  end

  it "renders a list of categories" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
