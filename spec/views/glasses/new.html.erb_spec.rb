require 'spec_helper'

describe "glasses/new" do
  before(:each) do
    assign(:glass, stub_model(Glass,
      :type => "",
      :cost => "9.99",
      :markup => 1.5,
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new glass form" do
    render

    assert_select "form[action=?][method=?]", glasses_path, "post" do
      assert_select "input#glass_type[name=?]", "glass[type]"
      assert_select "input#glass_cost[name=?]", "glass[cost]"
      assert_select "input#glass_markup[name=?]", "glass[markup]"
      assert_select "input#glass_value[name=?]", "glass[value]"
    end
  end
end
