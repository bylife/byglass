require 'spec_helper'

describe "sales/show" do
  before(:each) do
    @sale = assign(:sale, stub_model(Sale,
      :customer_id => "",
      :user_id => "",
      :value => 1.5,
      :delivered => false,
      :observations => "Observations",
      :payment_method => 1,
      :payed => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Observations/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/false/)
  end
end
