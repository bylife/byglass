require 'spec_helper'

describe "sales/index" do
  before(:each) do
    assign(:sales, [
      stub_model(Sale,
        :customer_id => "",
        :user_id => "",
        :value => 1.5,
        :delivered => false,
        :observations => "Observations",
        :payment_method => 1,
        :payed => false
      ),
      stub_model(Sale,
        :customer_id => "",
        :user_id => "",
        :value => 1.5,
        :delivered => false,
        :observations => "Observations",
        :payment_method => 1,
        :payed => false
      )
    ])
  end

  it "renders a list of sales" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Observations".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
