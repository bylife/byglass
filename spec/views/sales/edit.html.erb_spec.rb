require 'spec_helper'

describe "sales/edit" do
  before(:each) do
    @sale = assign(:sale, stub_model(Sale,
      :customer_id => "",
      :user_id => "",
      :value => 1.5,
      :delivered => false,
      :observations => "MyString",
      :payment_method => 1,
      :payed => false
    ))
  end

  it "renders the edit sale form" do
    render

    assert_select "form[action=?][method=?]", sale_path(@sale), "post" do
      assert_select "input#sale_customer_id[name=?]", "sale[customer_id]"
      assert_select "input#sale_user_id[name=?]", "sale[user_id]"
      assert_select "input#sale_value[name=?]", "sale[value]"
      assert_select "input#sale_delivered[name=?]", "sale[delivered]"
      assert_select "input#sale_observations[name=?]", "sale[observations]"
      assert_select "input#sale_payment_method[name=?]", "sale[payment_method]"
      assert_select "input#sale_payed[name=?]", "sale[payed]"
    end
  end
end
