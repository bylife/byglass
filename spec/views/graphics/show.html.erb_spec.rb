require 'spec_helper'

describe "graphics/show" do
  before(:each) do
    @graphic = assign(:graphic, stub_model(Graphic,
      :title => "Title",
      :path => "Path"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Path/)
  end
end
