require 'spec_helper'

describe "graphics/edit" do
  before(:each) do
    @graphic = assign(:graphic, stub_model(Graphic,
      :title => "MyString",
      :path => "MyString"
    ))
  end

  it "renders the edit graphic form" do
    render

    assert_select "form[action=?][method=?]", graphic_path(@graphic), "post" do
      assert_select "input#graphic_title[name=?]", "graphic[title]"
      assert_select "input#graphic_path[name=?]", "graphic[path]"
    end
  end
end
