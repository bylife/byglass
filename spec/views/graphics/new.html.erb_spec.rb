require 'spec_helper'

describe "graphics/new" do
  before(:each) do
    assign(:graphic, stub_model(Graphic,
      :title => "MyString",
      :path => "MyString"
    ).as_new_record)
  end

  it "renders new graphic form" do
    render

    assert_select "form[action=?][method=?]", graphics_path, "post" do
      assert_select "input#graphic_title[name=?]", "graphic[title]"
      assert_select "input#graphic_path[name=?]", "graphic[path]"
    end
  end
end
