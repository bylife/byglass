require 'spec_helper'

describe "graphics/index" do
  before(:each) do
    assign(:graphics, [
      stub_model(Graphic,
        :title => "Title",
        :path => "Path"
      ),
      stub_model(Graphic,
        :title => "Title",
        :path => "Path"
      )
    ])
  end

  it "renders a list of graphics" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Path".to_s, :count => 2
  end
end
