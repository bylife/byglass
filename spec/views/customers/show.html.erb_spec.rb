require 'spec_helper'

describe "customers/show" do
  before(:each) do
    @customer = assign(:customer, stub_model(Customer,
      :name => "Name",
      :email => "Email",
      :address => "Address",
      :phone => "Phone",
      :cellphone => "Cellphone",
      :cpf => "Cpf",
      :rg => "Rg",
      :created_at => "Created At",
      :updated_at => "Updated At"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Cellphone/)
    expect(rendered).to match(/Cpf/)
    expect(rendered).to match(/Rg/)
    expect(rendered).to match(/Created At/)
    expect(rendered).to match(/Updated At/)
  end
end
