require 'spec_helper'

describe "customers/edit" do
  before(:each) do
    @customer = assign(:customer, stub_model(Customer,
      :name => "MyString",
      :email => "MyString",
      :address => "MyString",
      :phone => "MyString",
      :cellphone => "MyString",
      :cpf => "MyString",
      :rg => "MyString",
      :created_at => "MyString",
      :updated_at => "MyString"
    ))
  end

  it "renders the edit customer form" do
    render

    assert_select "form[action=?][method=?]", customer_path(@customer), "post" do
      assert_select "input#customer_name[name=?]", "customer[name]"
      assert_select "input#customer_email[name=?]", "customer[email]"
      assert_select "input#customer_address[name=?]", "customer[address]"
      assert_select "input#customer_phone[name=?]", "customer[phone]"
      assert_select "input#customer_cellphone[name=?]", "customer[cellphone]"
      assert_select "input#customer_cpf[name=?]", "customer[cpf]"
      assert_select "input#customer_rg[name=?]", "customer[rg]"
      assert_select "input#customer_created_at[name=?]", "customer[created_at]"
      assert_select "input#customer_updated_at[name=?]", "customer[updated_at]"
    end
  end
end
