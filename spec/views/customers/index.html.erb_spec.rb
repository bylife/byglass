require 'spec_helper'

describe "customers/index" do
  before(:each) do
    assign(:customers, [
      stub_model(Customer,
        :name => "Name",
        :email => "Email",
        :address => "Address",
        :phone => "Phone",
        :cellphone => "Cellphone",
        :cpf => "Cpf",
        :rg => "Rg",
        :created_at => "Created At",
        :updated_at => "Updated At"
      ),
      stub_model(Customer,
        :name => "Name",
        :email => "Email",
        :address => "Address",
        :phone => "Phone",
        :cellphone => "Cellphone",
        :cpf => "Cpf",
        :rg => "Rg",
        :created_at => "Created At",
        :updated_at => "Updated At"
      )
    ])
  end

  it "renders a list of customers" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Cellphone".to_s, :count => 2
    assert_select "tr>td", :text => "Cpf".to_s, :count => 2
    assert_select "tr>td", :text => "Rg".to_s, :count => 2
    assert_select "tr>td", :text => "Created At".to_s, :count => 2
    assert_select "tr>td", :text => "Updated At".to_s, :count => 2
  end
end
