require 'spec_helper'

describe "dimensions/new" do
  before(:each) do
    assign(:dimension, stub_model(Dimension,
      :product => nil,
      :product_id => 1,
      :name => "MyString",
      :unity => "MyString",
      :value => "9.99"
    ).as_new_record)
  end

  it "renders new dimension form" do
    render

    assert_select "form[action=?][method=?]", dimensions_path, "post" do
      assert_select "input#dimension_product[name=?]", "dimension[product]"
      assert_select "input#dimension_product_id[name=?]", "dimension[product_id]"
      assert_select "input#dimension_name[name=?]", "dimension[name]"
      assert_select "input#dimension_unity[name=?]", "dimension[unity]"
      assert_select "input#dimension_value[name=?]", "dimension[value]"
    end
  end
end
