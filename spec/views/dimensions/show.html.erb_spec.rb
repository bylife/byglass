require 'spec_helper'

describe "dimensions/show" do
  before(:each) do
    @dimension = assign(:dimension, stub_model(Dimension,
      :product => nil,
      :product_id => 1,
      :name => "Name",
      :unity => "Unity",
      :value => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Unity/)
    expect(rendered).to match(/9.99/)
  end
end
