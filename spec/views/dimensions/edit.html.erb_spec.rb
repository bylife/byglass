require 'spec_helper'

describe "dimensions/edit" do
  before(:each) do
    @dimension = assign(:dimension, stub_model(Dimension,
      :product => nil,
      :product_id => 1,
      :name => "MyString",
      :unity => "MyString",
      :value => "9.99"
    ))
  end

  it "renders the edit dimension form" do
    render

    assert_select "form[action=?][method=?]", dimension_path(@dimension), "post" do
      assert_select "input#dimension_product[name=?]", "dimension[product]"
      assert_select "input#dimension_product_id[name=?]", "dimension[product_id]"
      assert_select "input#dimension_name[name=?]", "dimension[name]"
      assert_select "input#dimension_unity[name=?]", "dimension[unity]"
      assert_select "input#dimension_value[name=?]", "dimension[value]"
    end
  end
end
