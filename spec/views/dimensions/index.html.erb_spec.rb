require 'spec_helper'

describe "dimensions/index" do
  before(:each) do
    assign(:dimensions, [
      stub_model(Dimension,
        :product => nil,
        :product_id => 1,
        :name => "Name",
        :unity => "Unity",
        :value => "9.99"
      ),
      stub_model(Dimension,
        :product => nil,
        :product_id => 1,
        :name => "Name",
        :unity => "Unity",
        :value => "9.99"
      )
    ])
  end

  it "renders a list of dimensions" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Unity".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
