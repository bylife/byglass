require 'spec_helper'

describe "measures/new" do
  before(:each) do
    assign(:measure, stub_model(Measure,
      :title => "MyString",
      :value => 1.5,
      :unity => "MyString"
    ).as_new_record)
  end

  it "renders new measure form" do
    render

    assert_select "form[action=?][method=?]", measures_path, "post" do
      assert_select "input#measure_title[name=?]", "measure[title]"
      assert_select "input#measure_value[name=?]", "measure[value]"
      assert_select "input#measure_unity[name=?]", "measure[unity]"
    end
  end
end
