require 'spec_helper'

describe "measures/show" do
  before(:each) do
    @measure = assign(:measure, stub_model(Measure,
      :title => "Title",
      :value => 1.5,
      :unity => "Unity"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/Unity/)
  end
end
