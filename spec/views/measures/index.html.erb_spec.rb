require 'spec_helper'

describe "measures/index" do
  before(:each) do
    assign(:measures, [
      stub_model(Measure,
        :title => "Title",
        :value => 1.5,
        :unity => "Unity"
      ),
      stub_model(Measure,
        :title => "Title",
        :value => 1.5,
        :unity => "Unity"
      )
    ])
  end

  it "renders a list of measures" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Unity".to_s, :count => 2
  end
end
