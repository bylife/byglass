require 'spec_helper'

describe "measures/edit" do
  before(:each) do
    @measure = assign(:measure, stub_model(Measure,
      :title => "MyString",
      :value => 1.5,
      :unity => "MyString"
    ))
  end

  it "renders the edit measure form" do
    render

    assert_select "form[action=?][method=?]", measure_path(@measure), "post" do
      assert_select "input#measure_title[name=?]", "measure[title]"
      assert_select "input#measure_value[name=?]", "measure[value]"
      assert_select "input#measure_unity[name=?]", "measure[unity]"
    end
  end
end
